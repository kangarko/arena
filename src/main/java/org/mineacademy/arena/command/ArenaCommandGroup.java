package org.mineacademy.arena.command;

import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.ReloadCommand;
import org.mineacademy.fo.command.SimpleCommandGroup;

/**
 * The main /arena command
 */
@AutoRegister
public class ArenaCommandGroup extends SimpleCommandGroup {

	@Override
	protected void registerSubcommands() {

		// Auto-register all sub command
		registerSubcommand(ArenaSubCommand.class);

		// Register the premade reload command from Foundation, remove if you want to have your own
		registerSubcommand(new ReloadCommand());
	}
}
