package org.mineacademy.arena;

import org.mineacademy.arena.model.ArenaClass;
import org.mineacademy.arena.model.ArenaListener;
import org.mineacademy.arena.model.ArenaManager;
import org.mineacademy.arena.model.ArenaPlayer;
import org.mineacademy.arena.model.ArenaReward;
import org.mineacademy.arena.model.ArenaStopReason;
import org.mineacademy.arena.model.ArenaTeam;
import org.mineacademy.arena.model.dm.DeathmatchArena;
import org.mineacademy.arena.model.eggwars.EggWarsArena;
import org.mineacademy.arena.model.monster.MobArena;
import org.mineacademy.arena.model.team.ctf.CaptureTheFlagArena;
import org.mineacademy.arena.model.team.tdm.TeamDeathmatchArena;
import org.mineacademy.arena.task.EscapeTask;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.plugin.SimplePlugin;

/**
 * The core plugin class for Arena
 */
public final class ArenaPlugin extends SimplePlugin {

	/**
	 * Load the plugin and its configuration
	 */
	@Override
	protected void onPluginStart() {

		// Register our arenas
		ArenaManager.registerArenaType(MobArena.class);
		ArenaManager.registerArenaType(DeathmatchArena.class);
		ArenaManager.registerArenaType(TeamDeathmatchArena.class);
		ArenaManager.registerArenaType(CaptureTheFlagArena.class);
		ArenaManager.registerArenaType(EggWarsArena.class);

		// Connect to MySQL
		// TODO Change this to be your own MySQL credentials, this won't work!
		//ArenaDatabase.start("mysql57.websupport.sk", 3311, "projectorion", "projectorion", "Te7=cXvxQI");

		// Enable messages prefix
		Common.setTellPrefix("[Arena]");

		Common.runLater(ArenaManager::loadArenas); // Uncomment this line if your arena world is loaded by a third party plugin such as Multiverse
	}

	/**
	 * Called on startup and reload, load arenas
	 */
	@Override
	protected void onReloadablesStart() {
		//ArenaManager.loadArenas(); // Comment this line if your arena world is loaded by a third party plugin such as Multiverse
		ArenaClass.loadClasses();
		ArenaTeam.loadTeams();

		ArenaReward.getInstance(); // Loads the file
		ArenaPlayer.clearAllData();

		registerEvents(new ArenaListener());

		Common.runTimer(20, new EscapeTask());
	}

	/**
	 * Stop arenas on server stop
	 */
	@Override
	protected void onPluginStop() {
		ArenaManager.stopArenas(ArenaStopReason.PLUGIN);
	}

	/**
	 * Stop arenas on reload
	 */
	@Override
	protected void onPluginReload() {
		ArenaManager.stopArenas(ArenaStopReason.RELOAD);
		ArenaManager.loadArenas(); // Uncomment this line if your arena world is loaded by a third party plugin such as Multiverse
	}
}
