package org.mineacademy.arena.model.dm;

import org.mineacademy.arena.model.Arena;
import org.mineacademy.arena.model.ArenaSettings;

import lombok.Getter;

/**
 * Represents settings used in deathmatch arenas
 */
@Getter
public class DeathmatchSettings extends ArenaSettings {

	/**
	 * The list of all entrance locations in the arena
	 */
	private LocationList entrances;

	/**
	 * Create new arena settings
	 *
	 * @param arena
	 */
	public DeathmatchSettings(final Arena arena) {
		super(arena);
	}

	/**
	 * @see org.mineacademy.arena.model.ArenaSettings#onLoad()
	 */
	@Override
	protected void onLoad() {
		super.onLoad();

		this.entrances = getLocationList("Entrance_Locations");
	}

	/**
	 * @see org.mineacademy.arena.model.ArenaSettings#isSetup()
	 */
	@Override
	public boolean isSetup() {
		return super.isSetup() && entrances.size() >= getMaxPlayers();
	}

	@Override
	public void onSave() {
		this.set("Entrance_Locations", entrances);

		super.onSave();
	}
}
